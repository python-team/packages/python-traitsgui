Metadata-Version: 1.0
Name: TraitsGUI
Version: 3.6.0
Summary: Traits-capable windowing framework.
Home-page: http://code.enthought.com/projects/traits_gui
Author: ETS Developers
Author-email: enthought-dev@enthought.com
License: BSD
Download-URL: http://www.enthought.com/repo/ETS/TraitsGUI-3.6.0.tar.gz
Description: The TraitsGUI project contains a toolkit-independent GUI abstraction layer
        (known as Pyface), which is used to support the "visualization" features of
        the Traits package. Thus, you can write code in terms of the Traits API
        (views, items, editors, etc.), and let TraitsGUI and your selected toolkit
        and back-end take care of the details of displaying them.
        
        To display Traits-based user interfaces, in addition to the TraitsGUI project,
        you must install one of the following combinations of packages:
        
        - Traits, TraitsBackendWX, and wxPython
        - Traits, TraitsBackendQt, and PyQt
        
        Prerequisites
        -------------
        If you want to build TraitsGUI from source, you must first install
        `setuptools <http://pypi.python.org/pypi/setuptools/0.6c8>`_.
        
        
Platform: Windows
Platform: Linux
Platform: Mac OS-X
Platform: Unix
Platform: Solaris
Classifier: Development Status :: 5 - Production/Stable
Classifier: Intended Audience :: Developers
Classifier: Intended Audience :: Science/Research
Classifier: License :: OSI Approved :: BSD License
Classifier: Operating System :: MacOS
Classifier: Operating System :: Microsoft :: Windows
Classifier: Operating System :: OS Independent
Classifier: Operating System :: POSIX
Classifier: Operating System :: Unix
Classifier: Programming Language :: Python
Classifier: Topic :: Scientific/Engineering
Classifier: Topic :: Software Development
Classifier: Topic :: Software Development :: Libraries
